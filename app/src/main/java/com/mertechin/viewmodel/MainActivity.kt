package com.mertechin.viewmodel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mertechin.viewmodel.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
	private lateinit var binding : ActivityMainBinding
	private val viewModel: MainViewModel by viewModels()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = ActivityMainBinding.inflate(layoutInflater)
		setContentView(binding.root)

		withViewModel()
	}

	private fun withViewModel(){
		binding.btnPlus.setOnClickListener{
			mainIncrementCount()
		}
		binding.btnMinus.setOnClickListener{
			mainDecerementCount()
		}

		viewModel.vCounter.observe(this) {
			result -> binding.tvAngka.text = result.toString()
		}
	}

	private fun mainIncrementCount(){
		viewModel.incrementCount()
	}

	private fun mainDecerementCount(){
		viewModel.decrementCount()
	}
}

class MainViewModel : ViewModel() {
	val vCounter : MutableLiveData<Int> = MutableLiveData(0)

	fun incrementCount() {
		vCounter.postValue(vCounter.value?.plus(1))
	}

	fun decrementCount() {
		vCounter.value?.let {
			if (it > 0) {
				vCounter.postValue(vCounter.value?.minus(1))
			}
		}
	}
}